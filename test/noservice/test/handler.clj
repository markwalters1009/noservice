(ns noservice.test.handler
  (:require
   [clojure.java.io :as io]
   [clojure.test :refer [deftest is testing]]
   [crypto.random :as random]
   [kerodon.core :as kc :refer [fill-in follow-redirect press session]]
   [kerodon.test :refer [attr? has regex? status? text? value?]]
   [noservice.config :refer [default-configuration]]
   [noservice.handler :refer [make-app]]
   [noservice.main :refer [write-password-to-new-file]]
   [noservice.routes.auth :as auth]
   [peridot.core :refer [request]]))

(def ^:private test-passwd-file "target/tmp/noservice-testing.passwd")
(def ^:private testing-password (random/hex 256))

;; Inline code seems to work ok here -- but wonder if there's a more
;; recommended approach.
(.mkdirs (io/as-file "target/tmp"))
(io/delete-file test-passwd-file true)
(write-password-to-new-file testing-password test-passwd-file)

(defn- make-testing-app []
  (make-app #(cons {:password-file test-passwd-file}
                   (default-configuration))))

(defn- file-response? [r type]
  (and
   (= type (get-in r [:response :headers "Content-Type"]))
   (instance? java.io.File (get-in r [:response :body]))))

(defn- visit [s res & rest]
  (apply kc/visit s (str "https://localhost" res) rest))

(defn- authorized-session [app]
  (-> (session app)
      (visit "/login")
      (fill-in "password" testing-password)
      (press "login")))

(defmacro ^:private at-login-page [r]
  `(do
     (has ~r (attr? [:form] :action "/login"))
     (has ~r (value? "password" ""))))

(defmacro ^:private at-hello-page [r]
  `(do
     (has ~r (regex? ".*Welcome to notmuch.*Saved searches:.*top-folder.*"))))

(defmacro ^:private follow-https-redirect [r path]
  `(let [target# (follow-redirect ~r)]
     ;; If we make it here, we know there's a location -- check other bits.
     (let [url# (java.net.URL. (get-in ~r [:response :headers "Location"]))]
       (if (every? identity [(is (= "https" (.getProtocol url#)))
                             (is (= ~path (.getPath url#)))])
         target#
         (throw (IllegalArgumentException.
                 "Previous response was not an https redirect"))))))

(deftest https-redirection
  (let [app (make-testing-app)]
    (testing "anonymous https redirections"
      (doseq [x ["/" "/invalid" "/css/screen.css" "/login"]]
        (-> (session app) (kc/visit x) (follow-https-redirect x))))
    (testing "authorized https redirections"
      (doseq [x ["/" "/invalid" "/css/screen.css" "/login"]]
        (-> (authorized-session app)
            (kc/visit (str "http://localhost" x))
            (follow-https-redirect x))))))

(deftest cookie-attributes
  (let [app (make-testing-app)
        response (-> (session app) (visit "/"))
        [cookie] (get-in response [:response :headers "Set-Cookie"])]
    (is (re-find #"Secure" cookie))
    (is (re-find #"HttpOnly" cookie))))

(deftest test-restrictions
  (let [app (make-testing-app)]

    (testing "invalid route (anonymous)"
      (-> (session app) (visit "/invalid") follow-redirect at-login-page))

    (testing "invalid route (authorized)"
      (-> (authorized-session app) (visit "/invalid") (has (status? 404))))

    (testing "public routes (anonymous)"
      (let [s (session app)]
        (is (file-response? (visit s "/css/screen.css") "text/css"))
        (is (file-response? (visit s "/js/noservice.js") "text/javascript"))))

    (testing "public routes (authorized)"
      (let [s (authorized-session app)]
        (is (file-response? (visit s "/css/screen.css") "text/css"))
        (is (file-response? (visit s "/js/noservice.js") "text/javascript"))))

     (testing "restricted routes (anonymous)"
       (-> (session app) (visit "/") follow-redirect at-login-page)
       (-> (session app) (visit "/part") follow-redirect at-login-page)
       (-> (session app) (visit "/mid") follow-redirect at-login-page)
       (-> (session app) (visit "/search") follow-redirect at-login-page)
       (-> (session app) (visit "/show") follow-redirect at-login-page))

     (testing "login requires anti-forgery token"
       (-> (session app)
           (request "/login" :request-method :post
                    :scheme :https
                    :params {:password testing-password})
           (has (status? 403))))))

(deftest test-login
  (let [app (make-testing-app)]

    (testing "Normal login"
      (-> (authorized-session app) follow-redirect at-hello-page))

    (testing "Login to /"
      (-> (session app)
          (visit "/")
          follow-redirect
          (fill-in "password" testing-password)
          (press "login")
          follow-redirect
          at-hello-page))

    (testing "Login to invalid"
      (-> (session app)
          (visit "/invalid")
          follow-redirect
          (fill-in "password" testing-password)
          (press "login")
          follow-redirect
          (has (text? "Not Found"))))))
