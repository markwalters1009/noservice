(ns noservice.test.util
  (:require
   [clojure.test :refer [deftest is]]
   [noservice.util :as util]))


(defn make-disposition [filename filename*]
  {:headers {"Content-Disposition"
             (format "inline; filename=\"%s\"; filename*='UTF-8'%s;"
                     filename filename*)}})

(deftest content-disposition
  (is (= (util/content-disposition {} "foo")
         (make-disposition "foo" "%66%6f%6f")))
  (is (= (util/content-disposition {} "\n")
         (make-disposition "_" "%0a")))
  (is (= (util/content-disposition {} "\r")
         (make-disposition "_" "%0d")))
  (is (= (util/content-disposition {} "\"")
         (make-disposition "_" "%22"))))
