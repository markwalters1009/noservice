(ns noservice.test.main
  (:require
   [clojure.java.io :refer [delete-file]]
   [clojure.test :refer [deftest is use-fixtures]]
   [noir.util.crypt :as crypt]
   [noservice.config :as config]
   [noservice.main :as main])
  (:import [java.io File]
           [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(use-fixtures :once (fn [f] (f) (binding [*out* *err*] (flush)) (flush)))

(deftest passwd
  (let [tmpdir (Files/createTempDirectory (.toPath (File. ".")) "test-passwd"
                                          (into-array FileAttribute []))]
    (try
      (let [pwfile (File. (.toFile tmpdir) "passwd")]
        (try
          (with-redefs [main/read-password #(identity "foo")]
            (is (zero? (main/passwd (.getAbsolutePath pwfile))))
            (is (crypt/compare "foo" (slurp pwfile))))
          (finally (delete-file pwfile true)))
        (try
          (with-redefs [main/read-password (let [x (atom 0)]
                                             #(str (swap! x inc)))]
            (is (not (zero? (main/passwd (.getAbsolutePath pwfile))))))
          (finally (delete-file pwfile true))))
      (finally (delete-file (.toFile tmpdir))))))

(deftest server-arguments
  (let [process-server-args #'main/process-server-args
        get-cc #(config/get-in (:config %) [:client-certs])]
    (is (get-cc (process-server-args [])))
    (is (get-cc (process-server-args ["--client-certs"])))
    (is (= false (get-cc (process-server-args ["--no-client-certs"]))))))
