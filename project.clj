(defproject noservice "0.1.0-SNAPSHOT"
  :description "unsafe, toy web interface for notmuch"
  :url "https://git.debian.org/?p=users/rlb/noservice.git"
  :pedantic? :abort
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.89"]
                 [compojure "1.5.0"]
                 [hiccup "1.0.5"]
                 [javax.servlet/javax.servlet-api "3.1.0"]
                 [lib-noir "0.9.9" :exclusions [javax.servlet/servlet-api]]
                 ;; This should likely match the jetty-server version.
                 ;; Check "lein deps :tree".
                 [org.eclipse.jetty/jetty-servlets "9.2.10.v20150310"]
                 [prismatic/dommy "1.1.0"]
                 [ring "1.4.0"]
                 [ring-server "0.4.0" :exclusions [ring ring-refresh]]
                 [ring/ring-anti-forgery "1.0.0"]
                 [ring/ring-ssl "0.2.1"]
                 [me.raynes/conch "0.8.0"]
                 [com.draines/postal "2.0.1"]
                 [javax.mail/mail "1.4.7"]]
  :plugins [[lein-cljsbuild "1.1.3"]]
  :cljsbuild {:builds [{:source-paths ["cljs"]
                        :compiler {:output-to "target/pub/js/noservice.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]}
  :hooks [leiningen.cljsbuild]
  :aot :all
  :main ^:skip-aot noservice.main
  :profiles
  {:production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}}
   :dev
   {:dependencies [[crypto-random "1.2.0"]
                   [kerodon "0.7.0"]]}})
