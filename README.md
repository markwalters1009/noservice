# noservice

Possibly sketchy web interface for [notmuch](http://notmuchmail.org).

While we know of no vulnerabilities when noservice is configured to
require client certificates (the default), its security has not
been evaluated by experts.

## Prerequisites

You will need a
[Java Development Kit](http://openjdk.java.net/install) version 7 or
newer, and [Leiningen](http://github.com/technomancy/leiningen)
version 1.7.0 or newer.

If you're on a fairly recent Debian system, then you should be able to
install them by running this (as root):

  ```sh
  apt-get install openjdk-7-jdk
  apt-get install leiningen
  ```

If you'd like to run a more recent Leiningen than what's available in
Debian (and you might), then as the user that you want to use to build
noservice, you should be able to copy the upstream lein script to
~/bin/ (see the upstream instructions), and after that, you should be
able to upgrade Leiningen whenever you like by running:


  ```sh
  lein upgrade
  ```

as the same user.

Though note that the default Debian shell configuration only includes
~/bin if it exists when you log in, so you might have to log out and
log back in for a new ~/bin/ directory to be noticed.

You may also want to add this (until adopted upstream) to your
~/.lein/profiles.clj file, to enable https for package retrievals from
the central archive:

  ```clojure
  {:user
   {:mirrors {"central" {:name "central/https"
                         :url "https://repo1.maven.org/maven2"}}
  ```

Further information can be found
[here](http://central.stage.sonatype.org/pages/consumers.html#leiningen).

## Running

We currently require https, so if you don't have a keystore you want
to use (doesn't everyone?), run this in the top level of the source
tree:

  ```sh
  ./create-keystore
  ```

This will create ./nostore.jks, and ./nostore.keypass -- protect both.
They should have been created with very restrictive permissions.

However, for now assume that the key generation and https support is
completely untrustworthy.  It needs thorough, qualified review.

We also require a password.  To set the initial password, run this:

  ```sh
  lein trampoline run passwd
  ```

Client certificate verification is enabled by default, so all clients
must present a certificate approved by the server before they can even
reach the password prompt.
[doc/client-certs.md](./doc/client-certs.md) describes the
configuration process.

You can disable client-certificate verification with the command line
option --no-client-certs.  This is not a recommended configuration,
and may be insecure unless you have other security measures in place
(e.g. an ssh tunnel), but could be useful for testing.

To start a web server for the application, run:

  ```sh
  lein run server
  ```

By default the server will listen on http://localhost:3000 and
https://localhost:3443, with all connections to the former
automatically redirected to the latter.

Note that right now, there is no automatic logout.  Once logged in,
you will remain logged in until you explicitly logout.  Furthermore,
logins are per-session and independent, so you can be simultaneously
logged in from multiple browsers.

To configure noservice, you can specify a config file (or files) with
--config:

  ```sh
  lein run server --config ./config.edn
  ```

When multiple config files are specified, values in files further to
the right on the command line shadow (override) those in files further
to the left.

Here's the default config, and all items are optional:

  ```clojure
  ;; -*-clojure-*-
  {:listen {:address "localhost" :port 3000 :ssl-port 3443 }
   :access-log {:retain-days 7 :path-prefix "./access-log."}
   :saved-searches
   [{:name "top-folder" :query "folder:"}
    {:name "today" :query  "date:1d.."}
    {:name "past 7 days" :query  "date:7d.."}
    {:name "past 31 days" :query  "date:31d.."}]}
  ```

If you have trouble with the lein commands you might want to try
cleaning the source tree first, i.e.:

  ```sh
  lein clean
  ```

## Sending Email

There is preliminary support for composing and replying to email. For
configuration see below.  The compose form is accessible via the
compose button in the top menu and there is a reply button on each
message in show mode.

Note that, when composing, all "to" fields in the form are joined to
make a single header, as are all "cc" fields, so you can choose
whether to add one address per field or multiple comma separated
addresses.

### Configuation

To use add the following settings to your config.edn file:

  * :mail-host specifies the host settings to use.

    If :mail-host is not set then the local sendmail is used.  In this
    case postal uses a hardcoded list of paths to find sendmail.  If
    you need to use a different path, set the SENDMAIL environment
    variable to the correct path before starting the server.

    For remote servers a typical setting might be the map

      ```clojure
      {:host "smtp.gmail.com"
       :user "username@gmail.com"
       :pass "password"
       :ssl :yes}
      ```

    See the postal page https://github.com/drewr/postal for full
    details.

  * :envelope-from specifies the address to use as the envelope-from
    address. If this is not set then the address part of the from
    address chosen will be used.

  * :bcc-address specifies an optional address to bcc all messages
    to. This is useful both as a sent mail store and as a confirmation
    the message actually got sent.

### Address book

noservice has some very basic address book support.

To use add a :address-book field to config.edn. The format should be
:address-book [a1 a2 a3...]

Each of the a's can either be an address (including real name if you
like) or a vector [alias, address]. In the latter case you type to
complete the alias but when selected the browser fills in the actual
address.

### Known Issues

* If the send fails the message is not saved anywhere. It may be
  recoverable by using back in the browser.

* There is no support for attachments.

* There is no reply-to-sender option.

* Some headers are all lower case (which is unusual but valid).

* Some versions of msmtp don't like postal's calling method: I think
  1.4.32 is OK but 1.4.31 and before *will* fail.

## License

This project is free software; you can redistribute it and/or modify
it under the terms of (at your option) either of the following two
licences:

  1) The GNU Lesser General Public License as published by the Free
     Software Foundation; either version 2.1, or (at your option) any
     later version

  2) The Eclipse Public License; either version 1.0 or (at your
     option) any later version.

Copyright © 2014 Rob Browning <rlb@defaultvalue.org>  
Copyright © 2014 Mark Walters <markwalters1009@gmail.com>  

### License exceptions

Notwithstanding the above declaration, any files in
src/noservice/patch/ are covered under the licenses and copyrights
listed at the top of the files.
