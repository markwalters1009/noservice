SSL Client Certification
========================

Client certificates are a way of authenticating a client in addition
to or instead of a password (SSL mutual or client authentication).
Once enabled, a client won't even be able to connect to the server
unless it presents an acceptable certificate.

To configure this you must have a Certificate Authority (CA), which
will be used to sign (validate) client certificates, and you must put
it in the truststore.jks keystore in the noservice root directory.
This keystore must be protected by the password already stored in
noservice.keypass.

The following details one way of achieving this.  The intention is to
keep the process similar to the handling of ssh keys.

1) First we create a self signed certificate (i.e. a keypair) on the
   client.

    openssl req -x509 -newkey rsa:2048 -nodes -keyout noservice-client.key \
                -days 365 -out noservice-client.crt

   Answer the questions as you like: I think the only important one is
   the CN (Common Name) which should be different from that of any
   other certificates you are using.

2) Make the keypair into a keyfile that firefox (and probably other
   browsers) can understand

     openssl pkcs12 -export -out noservice-client.p12 \
             -inkey noservice-client.key -in noservice-client.crt

   You will be asked for a password which will be needed in the next
   step.  (By default it must be at least 4 characters.  More is
   certainly recommended.)

3) Import the certificate into firefox:

    - Select the menu item preferences->advanced->certificates->view
      certificates->Your certificates->import.

    - Choose the file noservice-client.p12 generated above and enter
      the password you chose there.

3b) If you want a phone (using mobile firefox) to be able to connect,
    transfer the p12 generated in Step 2 to the phone.  However, you
    need to install an extension to install the certificate into
    firefox.  I use
    https://addons.mozilla.org/en-GB/firefox/addon/addcertificate/

4) Import the certificate to the noservice server.  Run the following
   in the base noservice directory (note you only need the
   noservice-client-crt file: you do not need to transfer the
   noservice-client.key to the server)

     keytool -import -keystore truststore.jks  -file noservice-client.crt \
             -storepass:file noservice.keypass -alias noservice-client

   The alias must be different from any other alias you have in the
   truststore.  Specifying the CN you chose above would be one
   possibility.

5) Enjoy.

If Firefox presents an error message during verification that says
"Secure Connection Failed", it may look like you (as the client) don't
trust the server, but in fact it's the server that doesn't trust you.

### Revocation

Whenever you want to revoke an authorisation you can just delete the
corresponding client certificate from the truststore, and restart
noservice.

     keytool -delete -keystore truststore.jks -storepass:file \
             noservice.keypass -alias <alias-to-remove>
