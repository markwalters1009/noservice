(ns noservice.views.layout
  (:require
   [hiccup.page :refer [html5 include-css include-js]]
   [hiccup.util :refer [url]]
   [hiccup.form :refer [form-to text-field]]
   [hiccup.element :refer [link-to]]
   [noir.session :as session]))

(defn- user-menu []
  (let [user (session/get :user)
        menu-link (fn [id target name]
                    (link-to {:class "menu-link" :id id} (url target) name))
        menu [:span {:class (str "menu" (when-not user " disabled"))}
              (menu-link "menu-item-hello" "/" "Hello")
              (menu-link "menu-item-settings" "/settings" "Settings")
              (menu-link "menu-item-compose" "/compose" "Compose ")
              (menu-link "menu-item-logout" "/logout" "Logout")
              (text-field (conj {:id "menu-search-bar" :placeholder "Search"}
                                (when-not user [:disabled "disabled"]))
                          "query" "")]]
    [:div
     (if user
       (form-to [:get "/search"] menu)
       menu)
     [:hr {:class "menu-rule"}]]))

(defn common [& body]
  (html5
    [:head
     [:title "noservice"]
     [:meta {:name "viewport" :content "width=device-width"}]
     (include-css "/css/screen.css")
     (include-js "/js/noservice.js")]
    [:body
     (user-menu)
     body]))
