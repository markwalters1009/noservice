(ns noservice.util
  (:require
   [clojure.string :as str]
   [hiccup.core :refer [h]]
   [ring.util.response :as r]))

(defn remove-vals
  "Returns a map containing only the entries in map for which (pred
  value) is true"
  [pred map]
  (into {} (remove (comp pred second) map)))

(defn render-tags [tags]
  (interpose " " (for [tag (sort tags)]
                   ;; FIXME: do proper (html5 at least) tag name mangling.
                   ;; and maybe something else entirely...
                   [:span {:class (str "tag tag-" tag)}
                    (if (= tag "flagged")
                      "&#x2605;"  ; star
                      (h tag))])))

;; This content-disposition handling should roughly follow these
;; recommendations http://greenbytes.de/tech/webdav/rfc6266.html#rfc.section.D
;;
;; The current approach is lazy -- quash anything to undersore in
;; filename that's not one of the "normal" quoted-string characters,
;; and just hex encode filename* completely.

(def ^:private invalid-char-rx
  (re-pattern (str "[^"
                   (apply str (map #(format "\\x%02x" %)
                                   (concat [32 33] (range 35 127))))
                   "]")))

(defn- mangle-to-quoted-str [s]
  (str/replace s invalid-char-rx "_"))

(defn content-disposition [response filename]
  (if-not filename
    (r/header response "Content-Disposition" "inline;")
    (r/header response
              "Content-Disposition"
              (format "inline; filename=\"%s\"; filename*='UTF-8'%s;"
                      (mangle-to-quoted-str filename)
                      (apply str (for [i (.getBytes filename "utf-8")]
                                   (format "%%%02x" i)))))))
