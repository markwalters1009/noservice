(ns noservice.main
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :refer [writer delete-file as-file]]
   [clojure.string :as str]
   [noir.util.crypt :as crypt]
   [noservice.config :as config :refer [default-configuration]]
   [noservice.handler :refer [make-app]]
   [ring.adapter.jetty :refer [run-jetty]])
  (:import
   [java.nio ByteBuffer]
   [java.nio.file FileAlreadyExistsException Files StandardOpenOption]
   [java.nio.file.attribute PosixFilePermission PosixFilePermissions]
   [java.util HashSet]
   [org.eclipse.jetty.servlets.gzip GzipHandler]
   [org.eclipse.jetty.server NCSARequestLog]
   [org.eclipse.jetty.server.handler RequestLogHandler])
  (:gen-class))

(defn- msg [stream & items]
  (binding [*out* stream]
    (doseq [i items]
      (print i))
    (flush)))

(defn- usage [stream]
  (msg stream
       "Usage:\n"
       "  noservice server [--config EDN_FILE]\n"
       "                   [--client-certs]\n"
       "                   [--no-client-certs]...\n"
       "  noservice passwd [FILE]\n"))

(defn- help
  ([] (usage *out*) 0)
  ([_ & _] (usage *err*) 1))

(defn- private-file [path]
  (let [nio-path (.toPath (as-file path))
        options (HashSet. [StandardOpenOption/CREATE_NEW
                           StandardOpenOption/APPEND])
        perms (PosixFilePermissions/fromString "rw-------")
        attr (PosixFilePermissions/asFileAttribute perms)]
    (Files/newByteChannel nio-path options (into-array [attr]))))

(defn write-password-to-new-file [password path]
  (if-let [password-file (try (private-file path)
                              (catch FileAlreadyExistsException ex
                                false))]
    (with-open [password-file password-file]
      (try
        ;; FIXME: handle partial writes
        (.write password-file
                (ByteBuffer/wrap (.getBytes (crypt/encrypt password))))
        (catch Exception ex
          (delete-file path true)
          (throw ex))))))

(defn- read-password []
  (apply str (.. System console readPassword)))

(defn passwd
  ([] (passwd "noservice.passwd"))
  ([password-file-path]
     (let [exists-msg "File already exists; delete it for a new password\n"]
       (if (.exists (as-file password-file-path))
         (do (msg *err* exists-msg) 1)
         (let [_ (do (print "New noservice password: ") (flush))
               password (read-password)
               _ (do (print "Retype new noservice password: ") (flush))
               retype (read-password)]
           (if-not (= password retype)
             (do (msg *err* (format "Sorry, passwords do not match\n")) 1)
             (if (write-password-to-new-file password password-file-path)
               0
               (do (msg *err* exists-msg) 1))))))))

(defn- add-request-logging [server path-prefix retain-days]
  (.setHandler server
               (doto (RequestLogHandler.)
                 (.setRequestLog (doto (NCSARequestLog.)
                                   (.setFilename (str path-prefix "yyyy_mm_dd"))
                                   (.setFilenameDateFormat "yyyy-MM-dd")
                                   (.setRetainDays retain-days)
                                   (.setAppend true)
                                   (.setExtended true)
                                   (.setLogCookies false)
                                   (.setLogTimeZone "GMT")))
                 (.setHandler (.getHandler server)))))

(defn- add-response-compression [server]
  (let [types ["application/clojure"
               "application/javascript"
               "application/json"
               "application/xhtml+xml"
               "image/svg+xml"
               "text/css"
               "text/html"
               "text/javascript"
               "text/plain"
               "text/xml"]]
    (.setHandler server
                 (doto (new GzipHandler)
                   (.setMimeTypes (str/join "," types))
                   (.setHandler (.getHandler server))))))

(defn- process-server-args
  [command-line-args]
  (loop [config (default-configuration)
         args command-line-args]
    (if (seq args)
      (let [[opt & remainder] args]
        (case opt

          "--config"
          (if-let [[filename & remainder] remainder]
            (recur (cons (edn/read-string (slurp filename)) config)
                   remainder)
            (do (usage *err*) 1))

          "--client-certs"
          (recur (cons {:client-certs true} config) remainder)

          "--no-client-certs"
          (recur (cons {:client-certs false} config) remainder)

          (do (usage *err*) 1)))
      {:config config})))

(defn server
  [& command-line-args]
  (let [{:keys [config]} (process-server-args command-line-args)
        configure (fn [server]
                    (add-response-compression server)
                    (when (config/get-in config [:access-log])
                      (add-request-logging
                       server
                       (config/get-in config [:access-log :path-prefix])
                       (config/get-in config [:access-log :retain-days]))))
        opts {:host (config/get-in config [:listen :address])
              :port (config/get-in config [:listen :port])
              :ssl-port (config/get-in config [:listen :ssl-port])
              :keystore "noservice.jks"
              :key-password (slurp "noservice.keypass")
              :configurator configure}
        keystore (.getAbsolutePath (as-file "noservice.jks"))
        truststore (.getAbsolutePath (as-file "truststore.jks"))
        client-certs (config/get-in config [:client-certs])
        client-opts (when (and client-certs (.exists (as-file truststore)))
                      {:client-auth :need
                       :truststore truststore
                       :trust-password (slurp "noservice.keypass")})]

    (msg *err* "Using keystore " (pr-str keystore) ".\n")
    (if (and client-certs (not client-opts))
      (do
        (msg *err*
             "Can't read client certificates from " (pr-str truststore) ".\n"
             "Specify --no-client-certs (potentially INSECURE) to disable "
             "this check.\n")
        1)
      (do
        (if client-certs
          (msg *err* "Using truststore " (pr-str truststore) ".\n")
          (msg *err* "Client-certs disabled by user (potentially INSECURE)\n"))
        (msg *err* (format "Available at http://%s:%s and https://%s:%s\n"
                           (:host opts) (:port opts)
                           (:host opts) (:ssl-port opts)))
        (run-jetty (make-app #(do config)) (merge opts client-opts))
        0))))

(defn- main
  ([] (usage *out*) 1)
  ([command & opts]
     (case command
       "help" (help opts)
       "server" (apply server opts)
       "passwd" (apply passwd opts)
       (do (usage *out*) 1))))

(defn -main [& args]
  (let [status (try
                 (apply main args)
                 (finally
                   (shutdown-agents)
                   (binding [*out* *err*] (flush))
                   (flush)))]
    (System/exit status)))
