(ns noservice.handler
  (:require [compojure.core :refer [defroutes ANY]]
            [compojure.route :as route]
            [noir.response :refer [redirect]]
            [noir.session :as session]
            [noir.util.middleware :refer [app-handler]]
            [noir.util.route :refer [restricted]]
            [noservice.config :as config]
            [noservice.routes.auth :refer [make-auth-routes]]
            [noservice.routes.compose :refer [make-compose-routes]]
            [noservice.routes.home :refer [make-home-routes]]
            [noservice.routes.search :refer [make-search-routes]]
            [noservice.routes.show :refer [show-routes]]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [ring.middleware.defaults :refer [secure-site-defaults]]
            [ring.middleware.ssl :refer [wrap-ssl-redirect]]
            [ring.util.request :refer [request-url]]))

(defn- authorized-user? [req]
  (session/get :user))

(defn- redirect-to-login [req]
  (session/put! :redirect-after-login (request-url req))
  (redirect "/login"))

(defroutes app-routes
  ;; Serve resources/public/* as /*, without requiring a login
  (route/resources "/")
  ;; Final fallback
  (let [nope (route/not-found "Not Found")]
    (ANY "*" req (restricted (nope req)))))

(defn make-app [configuration]
  ;; Can't use noir's wrap-force-ssl because run-jetty doesn't let you
  ;; disable non-ssl ports.
  (app-handler [(make-auth-routes configuration)
                (make-home-routes configuration)
                (make-search-routes configuration)
                (make-compose-routes configuration)
                show-routes
                app-routes]

   ;; https://github.com/ring-clojure/ring-defaults/blob/master/src/ring/middleware/defaults.clj
   :ring-defaults
   (-> secure-site-defaults
       (assoc-in [:session :timeout] (* 60 30))
       (assoc-in [:session :timeout-response] (redirect "/"))
       (assoc-in [:security :ssl-redirect]
                 {:ssl-port
                  (config/get-in (configuration)
                                 [:listen :ssl-port])}))

   :access-rules [{:rule authorized-user? :on-fail redirect-to-login}]

   ;; What if any of these do we want?
   ;; serialize/deserialize the following data formats
   ;; available formats: :json :json-kw :yaml :yaml-kw :edn :yaml-in-html
   :formats [:json-kw :edn]))
