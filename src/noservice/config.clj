(ns noservice.config
  (:require
   [clojure.string :as str]
   [me.raynes.conch :refer [with-programs]])
  (:refer-clojure :exclude [get-in]))

(def ^{:private true} default-config
  {:client-certs true
   :listen {:address "localhost"
            :port 3000
            :ssl-port 3443}
   :password-file "noservice.passwd"
   :access-log {:path-prefix "./access-log." :retain-days 7}
   :saved-searches
   [{:name "top-folder" :query "folder:"}
    {:name "today" :query  "date:1d.."}
    {:name "past 7 days" :query  "date:7d.."}
    {:name "past 31 days" :query  "date:31d.."}]})

(defn default-configuration []
  [default-config])

(def ^:private unique-obj (Object.))

;; (defn contains-in? [coll ks]
;;   (if (map? coll)
;;     (not (identical? unique-obj (clojure.core/get-in coll ks unique-obj)))
;;     (contains? coll ks)))

(defn get-in
  "Returns the first value associated with ks (a sequence of keys)
  found in configuration (a sequence of nested associative
  structures). Returns nil if the key is not present, or the not-found
  value if supplied.  Similar to clojure.core/get-in."
  ([configuration ks] (get-in configuration ks nil))
  ([configuration ks not-found]
     (if-let [x (seq (remove (partial identical? unique-obj)
                             (map #(clojure.core/get-in % ks unique-obj)
                                  configuration)))]
       (first x)
       not-found)))

(defn notmuch-get
  "Get ITEM from the notmuch config. Returns a vector of the returned
  values."
  [item]
  (with-programs [notmuch]
    (str/split (notmuch "config" "get" item) #"\n")))
