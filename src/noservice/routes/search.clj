(ns noservice.routes.search
  (:require
   [clojure.edn :as edn]
   [clojure.string :as str]
   [compojure.core :refer [GET routes]]
   [hiccup.core :refer [h]]
   [hiccup.element :refer [link-to]]
   [hiccup.util :refer [url]]
   [me.raynes.conch :refer [with-programs]]
   [noir.util.route :refer [restricted]]
   [noservice.config :as config]
   [noservice.util :refer [render-tags]]
   [noservice.views.layout :as layout]))

;; FIXME: behavior, tests, error handling, everything...

(defn- summarize-threads [threads context]
  [:div {:id "thread-summary-list"}
   (for [x threads]
     (let [sc {:class "thread-summary-cell"}]
       (link-to
        {:class (str/join " " (cons "thread-summary-line"
                                    ;; FIXME: encoding?
                                    (map #(str "thread-is-" %) (:tags x))))}
        (url "/show" {:query (str "thread:" (:thread x)) :context context})
        [:span sc (h (:date_relative x))]
        [:span sc (format "[%d/%d]" (:matched x) (:total x))]
        [:span {:class "thread-summary-cell author-cell"}
         (h (:authors x))]
        [:span (merge sc {:style "overflow: hidden"})
         (h (:subject x)) " (" (render-tags (:tags x)) ")"])))])

(defn- search [query page configuration]
  (let [page (try (Integer/parseInt (or page "1")) (catch NumberFormatException e))]
    (layout/common
    (if-not page
      "Error: Page not a number"
      (with-programs [notmuch]
        (let [threads-per-page (config/get-in (configuration)
                                              [:threads-per-page] 20)
              offset (str "--offset=" (* (- page 1) threads-per-page))
              search (map #(let [x (apply hash-map %)] (assoc x :tags (set (:tags x))))
                          (edn/read-string (notmuch "search"
                                                    "--format=sexp"
                                                    (str "--limit=" threads-per-page)
                                                    offset
                                                    query)))
              more? (= (count search) threads-per-page)]

          (list (summarize-threads search query)
                [:hr {:class "menu-rule"}]
                [:div {:class "menu search-footer"}
                 [:span {:class "menu-link" :id "search-footer-newer"}
                  (if (> page 1)
                    (link-to {:title "Newer Messages"}
                             (url "/search" {:query query :page (dec page)})
                             (h "Newer"))
                    (h "Newer"))]
                 [:span {:class "menu-link" :id "search-footer-older"}
                  (if more?
                    (link-to {:title "Older Messages"}
                             (url "/search" {:query query :page (inc page)})
                             (h "Older" ))
                    (h "Older"))]])))))))

(defn make-search-routes [configuration]
  (routes
   (GET "/search" [query page] (restricted (search query page configuration)))))
