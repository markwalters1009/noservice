(ns noservice.routes.home
  (:require
   [clojure.string :as str]
   [compojure.core :refer [GET routes]]
   [hiccup.core :refer [h]]
   [hiccup.element :refer [link-to]]
   [hiccup.util :refer [url]]
   [me.raynes.conch :refer [with-programs]]
   [noir.util.route :refer [restricted]]
   [noservice.config :as config]
   [noservice.views.layout :as layout]))

;; FIXME: behavior, tests, error handling, everything...

(defn- strpart [n s]
  "Break a string into substrings of length n (or less for the first string)."
  (let [sn (count s)
        rsn (rem sn n)]
    (drop-while #(zero? (count %))
                (cons (subs s 0 rsn)
                      (map #(subs s % (min (+ % n) (count s)))
                           (range rsn (- sn rsn) n))))))

(defn- notmuch-formatted-counts [queries]
  "Format counts like notmuch-hello, i.e. 1 234 567."
  (with-programs [notmuch]
    (let [counts (try
                   (str/split
                    (notmuch "count" "--batch" {:in (str/join "\n" queries)})
                    #"\n")
                   (catch clojure.lang.ExceptionInfo ex
                     ;; FIXME
                     (prn ex)))]
      (map #(str/join " " (strpart 3 %)) counts))))

(defn- columnated-counts [items css-id]
  (let [count-width (* 0.7 (apply max (map #(count (:count %)) items)))
        col-width (* 0.8 (apply max (map #(+ (count (:count %))
                                             (count (:name %)))
                                         items)))]
    [:div {:id css-id
           :style (str "-moz-column-width: " col-width "em;"
                       "-webkit-column-width: " col-width "em;"
                       "column-width: " col-width "em;")}
     (for [{n :count name :name query :query} items]
       [:article
        [:span.columnated-count {:style (str "min-width: " count-width "em")}
         (h n)]
        [:span.columnated-name
         (link-to (url "/search" {:query query}) (h name))]])]))

(defn- render-saved-searches [configuration]
  (with-programs [notmuch]
    ;; FIXME: error handling (of course)
    (let [saved-searches (config/get-in (configuration) [:saved-searches])
          counts (notmuch-formatted-counts (map :query saved-searches))
          saved-searches (map #(assoc %1 :count %2) saved-searches counts)]
      (columnated-counts saved-searches "saved-searches"))))

(defn- render-all-tags []
  (with-programs [notmuch]
    (let [tags (str/split (notmuch "search" "--output=tags" "*") #"\n")
          counts (notmuch-formatted-counts (map #(str "is:" %) tags))
          tag-counts (map (fn [tag count]
                            {:name tag :count count :query (str "is:" tag)})
                          tags counts)]
      (columnated-counts tag-counts "all-tags"))))

(defn- home [configuration]
  (layout/common
   (with-programs [notmuch]
     (let [msg-count (str/join " " (strpart 3 (str/trimr (notmuch "count" "*"))))]
       (list
        [:p "Welcome to " [:a {:href "http://notmuchmail.org"} "notmuch"] "."
         "  You have " (h msg-count) " messages."]
        [:p "Saved searches:" (render-saved-searches configuration)]
        [:p "Recent searches:"]
        [:p "All tags:" (render-all-tags)])))))

(defn make-home-routes [configuration]
  (routes
   (GET "/" [] (restricted (home configuration)))
   (GET "/settings" [] (restricted (layout/common [:p "Not implemented yet."])))))
