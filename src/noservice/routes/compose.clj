(ns noservice.routes.compose
  (:require
   [clojure.string :as str]
   [compojure.core :refer [GET POST routes]]
   [hiccup.core :refer [h]]
   [hiccup.form :refer [form-to hidden-field text-field text-area]]
   [me.raynes.conch :refer [with-programs]]
   [noir.response :refer [redirect]]
   [noir.util.route :refer [restricted]]
   [noservice.config :as config]
   [noservice.parse :refer [parse-reply-sexp]]
   [noservice.util :refer [remove-vals render-tags]]
   [noservice.views.layout :as layout]
   [postal.core :refer [send-message]]
   [ring.util.anti-forgery :refer [anti-forgery-field]])
  (:import [javax.mail.internet InternetAddress]))

(defn parse-header
  "Parse a header string into a vector of addresses."
  [header-string]
  (map #(.toString %)
       (InternetAddress/parse header-string)))

(defn get-address
  "Parse a header string for an email address sans realname."
  [header-string]
  (.getAddress (first (InternetAddress/parse header-string))))

(defn from-addresses [header-string]
  "Return a vector of possible from-addresses by taking addresses in
  header-string followed by addresses in the notmuch config file."
  (let [primary (config/notmuch-get "user.primary_email")
        other (config/notmuch-get "user.other_email")
        real-name (first (config/notmuch-get "user.name"))
        addresses (map #(str/join [real-name " <" % ">"])
                       (concat primary other))]
    (if header-string
      (-> header-string
          parse-header
          (concat addresses)
          distinct)
      addresses)))

(defn get-sender [from configuration]
  "Return an envelope-from address. This is taken from the noservice
  config file; if not specified the from address (sans realname) is
  used."
  (or (config/get-in (configuration) [:envelope-from])
      (get-address from)))

(defn canonicalize-header-map [header-map]
  ;; XXX Postal wants lower case for special headers :to etc
  ;; but maybe we want some other case like In-Reply-To for
  ;; non-special headers. I believe MUA should be case
  ;; insensitive so it shouldn't matter.
  (zipmap
   (map (comp keyword str/lower-case name) (keys header-map))
   (vals header-map)))

(defn format-datalist-options
  "Insert DATA as a datalist escpaing as appropriate. Data should be
  a vector of elements. If an element is a single string that is
  inserted as the content for that option; if it is a vector then the
  first item is inserted as content and the second as the desired
  value."
  [data]
  (map (fn [x]
         (if (vector? x)
           ;; By default hiccup escapes attributes but not content.
           [:option {:value (second x)} (h (first x))]
           [:option (h x)]))
       data))

(defn address-book [configuration]
  [:datalist#address-book
   (format-datalist-options
    (config/get-in (configuration) [:address-book]))])

(defn- multi-value-header [name value datalist]
  (let [lc (str/lower-case name)]
    [:div.compose-header
     [:label.header-name
      [:a {:href "#" :onclick "noservice.core.append_header_BANG_(this)"} (h name)]]
     [:span.header-value
      (text-field {:class "header-sub-value"
                   :list datalist
                   :onkeypress "noservice.core.handle_header_event(event)"}
                  (str lc "[]") value)]]))

(defn compose
  "Set up a compose form with fields pre-filled based on the arguments"
  ([configuration] (compose nil nil configuration))
  ([hdrs body configuration]
     (layout/common
      (let [headers (canonicalize-header-map hdrs)
            {:keys [to cc subject]} headers
            from (from-addresses (:from headers))]
        (form-to {:id "compose-form"}
                 [:post "/send-email"]
                 (anti-forgery-field)
                 (hidden-field "other-headers" (pr-str headers))
                 [:datalist#noservice-from-addresses
                  (format-datalist-options from)]
                 (address-book configuration)
                 [:div#compose-headers
                  [:div.compose-header
                   [:label.header-name "From"]
                   (text-field {:class "header-value"
                                :list "noservice-from-addresses"}
                               "from" (first from))]
                  (multi-value-header "To" to "address-book")
                  (multi-value-header "Cc" cc "address-book")
                  [:div.compose-header [:label.header-name "Subject"]
                   (text-field {:class "header-value"} "subject" subject)]]
                 (text-area {:id "body"} "body" body)
                 [:div#send
                  [:a {:href "#" :onclick "noservice.core.compose_submit();"}
                   "Send"]])))))

(defn get-text-plain-part [part]
   (cond
    (= (:content-type part) "text/plain")
    part
    (.startsWith (:content-type part) "multipart/")
    (first (remove nil? (for [p (:content part)] (get-text-plain-part p))))))

;; FIXME allow reply to sender
(defn reply
  "Reply to message with ID."
  [id configuration]
  (with-programs [notmuch]
    (let [reply (parse-reply-sexp
                  (notmuch "reply"
                           "--format=sexp"
                           "--reply-to=all"
                           id))
          headers (:reply-headers reply)
          text (-> reply
                   :original
                   :body
                   get-text-plain-part
                   :content)
          cited-text (str "> "
                          (str/join "\n> " (str/split-lines text))
                          "\n")]
      (compose headers cited-text configuration))))

(defn send-email [from to cc subject body other-headers configuration]
  (layout/common
   ;; FIXME automate version in user-agent.
   (let [message (merge {:user-agent "noservice-0.01"}
                        (canonicalize-header-map (read-string other-headers))
                        {:from from
                         :sender (get-sender from configuration)
                         :to (flatten (map parse-header to))
                         :cc (flatten (map parse-header cc))
                         :bcc (config/get-in (configuration) [:bcc-address])
                         :subject subject
                         :body body})
         mail-host (config/get-in (configuration) [:mail-host])
         result (send-message mail-host message)]
     [:div
      (h (:message result))
      (h (format "(Exit code %d)" (:code result)))])))

(defn make-compose-routes [configuration]
  (routes
   (GET "/compose" [] (restricted (compose configuration)))
   (GET "/reply" [id] (restricted (reply id configuration)))
   (GET "/send-email" [] (restricted (redirect "/compose")))
   (POST "/send-email" [from to cc subject body other-headers]
         (restricted (send-email from to cc subject body other-headers configuration)))))
