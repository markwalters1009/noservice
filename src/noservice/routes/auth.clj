(ns noservice.routes.auth
  (:require
   [compojure.core :refer [GET POST routes]]
   [hiccup.form :refer [label form-to password-field submit-button text-field]]
   [noir.session :as session]
   [noir.response :refer [redirect]]
   [noir.util.crypt :as crypt]
   [noir.validation :refer [errors? has-value? on-error rule set-error]]
   [noservice.config :as config]
   [noservice.views.layout :as layout]
   [ring.util.anti-forgery :refer [anti-forgery-field]]))

(defn- current-time [] (.getTime (java.util.Date.)))

(defn login-page []
  (layout/common
   (form-to
    [:post "/login"]
    (anti-forgery-field)
    [:div#login-box
     [:div#login-box-header [:b "noservice"]]
     [:div.login-box-row
      (label {:class "login-box-text"} :password "password")
      (password-field {:autofocus "true"} :password)
      (submit-button "login")]
     (on-error :password
               (fn [[err]]
                 [:div.login-box-row
                  [:span {:class "login-box-text error-message"} err]]))])))

(def ^{:private true} too-soon-interval 5000) ;; in milliseconds

(def ^{:private true} last-failed-attempt
  (atom (- Long/MIN_VALUE (BigDecimal. too-soon-interval))))

(defn- too-soon? []
  (let [then @last-failed-attempt
        now (current-time)]
    (or (> then now)
        (< (- now then) too-soon-interval))))

(defn handle-login [password configuration]
  (if (too-soon?)
    (do
      (set-error :password
                 "recent login failures; please wait 5 seconds and retry")
      (login-page))
    (do
      (rule (has-value? password)
            [:password "password required"])
      (let [valid? (crypt/compare password
                                  (slurp (config/get-in (configuration)
                                                        [:password-file])))]
        (rule valid?
              [:password "invalid password"])
        (if (errors? :password)
          (do
            (when-not valid?
              (reset! last-failed-attempt (current-time)))
            (login-page))
          (do
            (session/put! :user "who else would it be?")
            (redirect (or (session/get! :redirect-after-login) "/"))))))))

(defn make-auth-routes [configuration]
  (routes
   (GET "/login" [] (login-page))
   (POST "/login" [password] (handle-login password configuration))
   (GET "/logout" [] (session/clear!) (redirect "/"))))
